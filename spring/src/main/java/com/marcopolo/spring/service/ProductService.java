package com.marcopolo.spring.service;


import com.marcopolo.spring.dao.ProductDAO;
import com.marcopolo.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductService {
    private final ProductDAO productService ;

    @Autowired
    public ProductService(ProductDAO productService)
    {
        this.productService = productService;
    }

    public List<Product> getList(){
        return productService.findAll();
    }
    public Product createProduct(Product p){
        return productService.save(p);
    }
    public Product editProduct(String id, Product product)
    {
        product.setId(id);
        return productService.save(product);
    }
    public void deleteProduct(String id){
        productService.deleteById(id);
    }
    public Product getDetails(String id){
        Product p = productService.findById(id)
                .orElse(null);
        return p;
    }
}
