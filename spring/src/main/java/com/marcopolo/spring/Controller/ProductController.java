package com.marcopolo.spring.Controller;

import com.marcopolo.spring.model.Product;
import com.marcopolo.spring.response.ProductListResponse;
import com.marcopolo.spring.response.SingleProductResponse;
import com.marcopolo.spring.response.util.Response;
import com.marcopolo.spring.service.ProductService;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@RequestMapping("/products")
@Validated
@RestController
public class ProductController {

    private final ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping
    public Response getAll() throws Exception {
        return Response.makeResponse(HttpStatus.OK, new ProductListResponse(service.getList()));
    }

    @GetMapping("{id}")
    public Response getDetails(@PathVariable("id") String id) {
        Product p = service.getDetails(id);
        Response resp = null;
        if(p == null)
            resp = Response.makeError(HttpStatus.NOT_FOUND,"Product not found");
        else
            resp = Response.makeResponse(HttpStatus.OK, new SingleProductResponse(p));
        return resp;
    }

    @PutMapping("{id}")
    public Response updateProduct(@PathVariable("id") String id, @Valid @RequestBody Product p){
        Product updated = service.editProduct(id, p);
        return Response.makeResponse(HttpStatus.OK, new SingleProductResponse(updated));
    }

    @PostMapping
    public Response createProduct(@Valid @RequestBody Product p) {
        Product created = service.createProduct(p);
        return Response.makeResponse(HttpStatus.OK, new SingleProductResponse(created));
    }

    @DeleteMapping("{id}")
    public Response deleteProduct(@PathVariable("id") String id){
        service.deleteProduct(id);
        return Response.makeResponse(HttpStatus.NO_CONTENT,null);
    }
}
