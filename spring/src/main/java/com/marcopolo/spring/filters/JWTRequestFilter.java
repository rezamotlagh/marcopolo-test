package com.marcopolo.spring.filters;

import com.auth0.jwt.interfaces.Claim;
import com.marcopolo.spring.dao.UserDAO;
import com.marcopolo.spring.model.User;
import com.marcopolo.spring.service.UserService;
import com.marcopolo.spring.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
public class JWTRequestFilter  extends OncePerRequestFilter {

    @Autowired
    private UserService userService;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    private UserDAO userDAO;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String auth = httpServletRequest.getHeader("Authorization");
        System.out.println(auth);
        if(auth!=null && auth.startsWith("Bearer ")){
            String id = null;
            String jwt = auth.substring(7);
            if(SecurityContextHolder.getContext().getAuthentication()==null && jwtUtil.isValid(jwt)){
                Map<String, Claim> claims = jwtUtil.getClaims(jwt);
                User u = userDAO.getUserById(claims.get("id").asString());
                if(u != null)
                {
                    UserDetails details = userService.loadUserByUsername(u.getUsername());
                    UsernamePasswordAuthenticationToken token= new UsernamePasswordAuthenticationToken(details,null,details.getAuthorities());
                    token.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(token);
                }
            }

        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
