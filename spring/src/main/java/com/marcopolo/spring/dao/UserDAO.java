package com.marcopolo.spring.dao;

import com.marcopolo.spring.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserDAO extends MongoRepository<User, String> {
    public User getUserByUsername(String username);
    public User getUserById(String id);
}
