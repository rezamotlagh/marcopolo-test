package com.marcopolo.spring.dao;

import com.marcopolo.spring.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductDAO extends MongoRepository<Product, String> {

}
