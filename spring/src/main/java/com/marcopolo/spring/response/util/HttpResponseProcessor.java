package com.marcopolo.spring.response.util;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class HttpResponseProcessor implements ResponseBodyAdvice {
    private boolean ok;
    private Object data;

    public HttpResponseProcessor() {
        super();
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (o instanceof Response) {
            SimpleBeanPropertyFilter simpleBeanPropertyFilter;
            if (((Response) o).isOk())
                simpleBeanPropertyFilter = SimpleBeanPropertyFilter.serializeAllExcept("message","status");
            else
                simpleBeanPropertyFilter = SimpleBeanPropertyFilter.serializeAllExcept("data","status");
            FilterProvider filterProvider = new SimpleFilterProvider()
                    .addFilter("responseFilter", simpleBeanPropertyFilter);

            MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(o);
            mappingJacksonValue.setFilters(filterProvider);
            HttpStatus status = HttpStatus.resolve(((Response) o).getStatus());
            serverHttpResponse.setStatusCode(status == null ? HttpStatus.OK : status);
            return mappingJacksonValue;
        }
        return o;
    }


    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
