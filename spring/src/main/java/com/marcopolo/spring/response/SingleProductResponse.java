package com.marcopolo.spring.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.marcopolo.spring.model.Product;

import java.io.Serializable;

public class SingleProductResponse implements Serializable {
    @JsonSerialize
    Product product;

    public SingleProductResponse(Product product) {
        this.product = product;
    }
}
