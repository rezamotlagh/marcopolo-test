package com.marcopolo.spring.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.marcopolo.spring.model.Product;

import java.io.Serializable;
import java.util.List;

public class ProductListResponse implements Serializable {
    @JsonSerialize
    List<Product> products;

    public ProductListResponse(List<Product> products) {
        this.products = products;
    }
}
