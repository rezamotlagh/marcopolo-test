package com.marcopolo.spring.response.util;

import com.fasterxml.jackson.annotation.JsonFilter;
import org.springframework.http.HttpStatus;

@JsonFilter("responseFilter")
public class Response {
    private boolean ok = false;
    private int status = 500;
    private Object data = null;
    private String message = "";

    private Response(){}

    public static Response makeError(HttpStatus status, String message){
        Response resp = new Response();
        resp.setOk(false);
        resp.setMessage(message);
        return resp;
    }
    public static Response makeResponse(HttpStatus status, Object data){
        Response resp = new Response();

        resp.setOk(true);
        resp.setStatus(status.value());
        resp.setData(data);
        return resp;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Response{" +
                "ok=" + ok +
                ", status=" + status +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}

