package com.marcopolo.spring.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.marcopolo.spring.Config;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class JwtUtil {
    public String extractUserId(String token) {
        return "username";
    }

    public boolean isTokenExpired(String token) {

        return false;
    }
    public boolean isTokenExpired(DecodedJWT jwt){
        System.out.println("Expired ... "+jwt.getExpiresAt().before(new Date()));
        return jwt.getExpiresAt().before(new Date());
    }
    public boolean isValid(String token) {
        Algorithm algorithm = Algorithm.HMAC256(Config.JWT_SECRET);
        JWTVerifier verifier = JWT.require(algorithm).build();
        try {
            DecodedJWT decodedJWT = verifier.verify(token);
            return !isTokenExpired(decodedJWT);
        } catch (JWTDecodeException e) {
            System.out.println(e);
        }
        return false;
    }

    public Map<String, Claim> getClaims(String token) throws JWTDecodeException {
        return JWT.decode(token).getClaims();
    }


}
