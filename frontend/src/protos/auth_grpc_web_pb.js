/**
 * @fileoverview gRPC-Web generated client stub for authentication
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.authentication = require('./auth_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.authentication.AuthServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.authentication.AuthServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.authentication.LoginRequest,
 *   !proto.authentication.UserReply>}
 */
const methodDescriptor_AuthService_login = new grpc.web.MethodDescriptor(
  '/authentication.AuthService/login',
  grpc.web.MethodType.UNARY,
  proto.authentication.LoginRequest,
  proto.authentication.UserReply,
  /**
   * @param {!proto.authentication.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.authentication.UserReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.authentication.LoginRequest,
 *   !proto.authentication.UserReply>}
 */
const methodInfo_AuthService_login = new grpc.web.AbstractClientBase.MethodInfo(
  proto.authentication.UserReply,
  /**
   * @param {!proto.authentication.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.authentication.UserReply.deserializeBinary
);


/**
 * @param {!proto.authentication.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.authentication.UserReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.authentication.UserReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.authentication.AuthServiceClient.prototype.login =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/authentication.AuthService/login',
      request,
      metadata || {},
      methodDescriptor_AuthService_login,
      callback);
};


/**
 * @param {!proto.authentication.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.authentication.UserReply>}
 *     Promise that resolves to the response
 */
proto.authentication.AuthServicePromiseClient.prototype.login =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/authentication.AuthService/login',
      request,
      metadata || {},
      methodDescriptor_AuthService_login);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.authentication.RegisterRequest,
 *   !proto.authentication.UserReply>}
 */
const methodDescriptor_AuthService_register = new grpc.web.MethodDescriptor(
  '/authentication.AuthService/register',
  grpc.web.MethodType.UNARY,
  proto.authentication.RegisterRequest,
  proto.authentication.UserReply,
  /**
   * @param {!proto.authentication.RegisterRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.authentication.UserReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.authentication.RegisterRequest,
 *   !proto.authentication.UserReply>}
 */
const methodInfo_AuthService_register = new grpc.web.AbstractClientBase.MethodInfo(
  proto.authentication.UserReply,
  /**
   * @param {!proto.authentication.RegisterRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.authentication.UserReply.deserializeBinary
);


/**
 * @param {!proto.authentication.RegisterRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.authentication.UserReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.authentication.UserReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.authentication.AuthServiceClient.prototype.register =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/authentication.AuthService/register',
      request,
      metadata || {},
      methodDescriptor_AuthService_register,
      callback);
};


/**
 * @param {!proto.authentication.RegisterRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.authentication.UserReply>}
 *     Promise that resolves to the response
 */
proto.authentication.AuthServicePromiseClient.prototype.register =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/authentication.AuthService/register',
      request,
      metadata || {},
      methodDescriptor_AuthService_register);
};


module.exports = proto.authentication;

