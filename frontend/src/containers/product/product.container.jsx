import React from 'react';
import {Component} from 'react'
import Product from "../../components/product/product.component";

class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }

    loadProducts() {
        const {user} = this.props
        fetch("http://localhost:4000/products/", {
            method: 'get',
            headers: {'Content-Type': 'application/json', 'Authorization': `Bearer ${user.accessToken}`}
        })
            .then(res => res.json())
            .then(resp => {
                console.log(resp)
                if (!resp.ok) {
                    alert(resp.message)
                    return
                }
                this.setState({products: resp.data.products})
            })
            .catch(alert)
    }

    componentDidMount() {
        this.loadProducts()

    }

    render() {
        const {products} = this.state
        return (
            <div>
                {
                    products.map((product, i) => {
                        return (
                            <Product
                                key={i}
                                description={product.description}
                                title={product.title}
                                image={product.image}
                            />
                        );
                    })
                }
            </div>
        );
    }
}

export default ProductList;