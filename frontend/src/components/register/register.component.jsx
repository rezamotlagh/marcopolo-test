import React from 'react';

const {AuthServiceClient} = require("../../protos/auth_grpc_web_pb.js")
const {RegisterRequest} = require("../../protos/auth_pb.js")

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            name: ''
        }
    }

    onNameChange = (event) => {
        this.setState({name: event.target.value})
    }

    onUsernameChange = (event) => {
        this.setState({username: event.target.value})
    }

    onPasswordChange = (event) => {
        this.setState({password: event.target.value})
    }

    onSubmitSignIn = () => {
        console.log("Click on sign in")
        let auth_client = new AuthServiceClient("http://localhost:8080", null, null)
        const req = new RegisterRequest()
        req.setUsername(this.state.username)
        req.setPassword(this.state.password)
        auth_client.register(req, {}, (err, resp) => {
            if (err) {
                alert(err.message)
                return;
            }
            this.props.loadUser({
                id: resp.getId(),
                username: resp.getUsername(),
                accessToken: resp.getAccesstoken()
            })
            this.props.onRouteChange("product")
        })
    }

    render() {
        return (
            <article className="br3 ba b--black-10 mv4 bg-white-10 w-100 w-50-m w-25-l mw6 shadow-5 center">
                <main className="pa4 white-40">
                    <div className="measure">
                        <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                            <legend className="f1 fw6 ph0 mh0">Register</legend>
                            <div className="mt3">
                                <label className="db fw6 lh-copy f6" htmlFor="username">Username</label>
                                <input
                                    className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                    type="text"
                                    name="username"
                                    id="username"
                                    onChange={this.onUsernameChange}
                                />
                            </div>
                            <div className="mv3">
                                <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                                <input
                                    className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                    type="password"
                                    name="password"
                                    id="password"
                                    onChange={this.onPasswordChange}
                                />
                            </div>
                        </fieldset>
                        <div className="">
                            <input
                                onClick={this.onSubmitSignIn}
                                className="b ph3 pv2 input-reset ba b--white-40 white-40 bg-transparent grow pointer f6 dib"
                                type="submit"
                                value="Register"
                            />
                        </div>
                    </div>
                </main>
            </article>
        );
    }
}

export default Register;