import React from 'react';
const { AuthServiceClient } = require("../../protos/auth_grpc_web_pb.js")
const { LoginRequest } = require("../../protos/auth_pb.js")

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
    }

    onUsernameChange = (event) => {
        this.setState({ username: event.target.value })
    }

    onPasswordChange = (event) => {
        this.setState({ password: event.target.value })
    }

    onSubmitSignIn = () => {
        let auth_client = new AuthServiceClient("http://localhost:8080", null, null)
        const req = new LoginRequest()
        req.setUsername(this.state.username)
        req.setPassword(this.state.password)
        auth_client.login(req, {}, (err, resp) => {
            if(err)
            {
                alert(err.message)
                return;
            }
            this.props.loadUser({
                id: resp.getId(),
                username: resp.getUsername(),
                accessToken: resp.getAccesstoken()
            })
            this.props.onRouteChange("product")
        })
    }

    render() {
        const { onRouteChange } = this.props;
        return (
            <article className="br3 ba b--white-05 bg-white-10 mv4 w-100 w-50-m w-25-l mw6 shadow-5 center">
                <main className="pa4 white-40">
                    <div className="measure">
                        <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                            <legend className="f1 fw6 ph0 mh0">Sign In</legend>
                            <div className="mt3">
                                <label className="db fw6 lh-copy f6" htmlFor="username">username</label>
                                <input
                                    className="pa2 input-reset ba bg-white-40 hover-bg-black hover-white w-100"
                                    type="text"
                                    name="username"
                                    id="username"
                                    onChange={this.onUsernameChange}
                                />
                            </div>
                            <div className="mv3">
                                <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                                <input
                                    className="b pa2 input-reset ba bg-white-40 hover-bg-black hover-white w-100"
                                    type="password"
                                    name="password"
                                    id="password"
                                    onChange={this.onPasswordChange}
                                />
                            </div>
                        </fieldset>
                        <div className="">
                            <input
                                onClick={this.onSubmitSignIn}
                                className="b ph3 pv2 input-reset ba b--white-05 bg-transparent grow white-40 pointer f6 dib"
                                type="submit"
                                value="Sign in"
                            />
                        </div>
                        <div className="lh-copy mt3">
                            <p onClick={() => onRouteChange('register')} className="f6 link dim white-40 db pointer">Register</p>
                        </div>
                    </div>
                </main>
            </article>
        );
    }
}

export default Login;