import React from 'react';
import {truncateString} from "../../helpers/strings.helpers";
import './product.style.css'
const Product = ({title, description, image}) => {
    return (
        <div className='tc grow bg-light-blue br3 pa3 ma2 dib bw2 shadow-5'>
            <img alt='robots' src={`${image}`} className='thumb'/>
            <div>
                <h3>{title}</h3>
                <p>{truncateString(description, 20)}</p>
            </div>
        </div>
    );
}

export default Product;