import logo from './logo.svg';
import {Component} from 'react'
import './App.css';
import Login from './components/login/login.component'
import Register from './components/register/register.component'
import ProductList from "./containers/product/product.container";
import 'tachyons'


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            route: "login",
            user: {
                id: '',
                accessToken: '',
                username: ''
            }
        }
    }

    loadUser = (user) => {
        this.setState({user: user})
        console.log(user);
    }
    onRouteChange = (path) => {
        this.setState({route: path})
    }


    render() {
        const {route, user, products} = this.state
        return (
            <div className="App">
                <header className="App-header">
                    {
                        route === 'login' ?
                            <Login loadUser={this.loadUser} onRouteChange={this.onRouteChange}/>
                            : (
                                route === 'register' ?
                                    <Register loadUser={this.loadUser} onRouteChange={this.onRouteChange}/>
                                    : <ProductList user={user}/>
                            )

                    }

                </header>
            </div>
        );
    }
}

export default App;
