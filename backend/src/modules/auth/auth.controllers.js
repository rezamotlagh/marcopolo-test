import getHttpError from '../../helpers/http-error.js'
import getHttpResponse from '../../helpers/http-response.js'
import makeUser from '../user/user.js'
import encoder from '../../helpers/jwt.js'
import {check as checkPassowrd} from '../../helpers/password-hasher.js'

export function makeLoginHandler({authModel}) {
    return async function handle(req) {
        if (!req.body) {
            return getHttpError({
                statusCode: 400,
                message: "Bad request."
            })
        }
        try {
            const userData = makeUser(req.body)
            const user = (await authModel.getUserByUsername(userData))
            if (!user) {
                return getHttpError({statusCode: 401, message: "Wrong username"})
            }
            // check password
            if (!checkPassowrd(userData.password, user.password))
                return getHttpError({statusCode: 401, message: "Wrong password"})
            return getHttpResponse({
                statusCode: 200,
                data: {
                    userInfo: {
                        id: user._id,
                        username: userData.username,
                        accessToken: encoder({id: user._id})
                    }
                }
            })
        } catch (e) {
            return getHttpError({
                message: e,
                statusCode: 401
            })
        }
    }
}


export function makeRegisterHandler({authModel}) {
    return async function handle(req) {
        if (!req.body) {
            return getHttpError({
                statusCode: 400,
                message: "Bad request."
            })
        }
        try {
            const user = makeUser({...req.body, hash_password: true})
            console.log(user)
            const result = await authModel.add(user)
            if (!result) {
                return getHttpError({status: 500, message: "Internal error"})
            }
            return getHttpResponse({
                statusCode: 200,
                data: {
                    userInfo: {
                        id: result._id,
                        username: user.username,
                        accessToken: encoder({id: user._id})
                        
                    }
                }
            })
        } catch (e) {
            return getHttpError({statusCode: 400, message: e})
        }
    }
}