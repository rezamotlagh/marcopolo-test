import makeUser from "../user/user.js";
import {check as checkPassword} from '../../helpers/password-hasher.js'
import * as grpc from "@grpc/grpc-js";
import getHttpError from "../../helpers/http-error.js";
import getHttpResponse from "../../helpers/http-response.js";
import encode from "../../helpers/jwt.js";

export default function createAuthCallback({authModel}) {
    return Object.freeze({
        login, register
    })

    async function login(call) {
        let userData;
        try {
            userData = makeUser(call.request);
            // authenticate the user

            const user = await authModel.getUserByUsername(userData);
            if (!user)
                return getHttpError({statusCode: 401, message: "Wrong username"})

            if (!checkPassword(userData.password, user.password))
                return getHttpError({statusCode: 401, message: "Wrong password"})
            // append access token to user

            return getHttpResponse({
                data: {
                    id: user._id,
                    username: user.username,
                    accessToken: encode({id: user._id})
                }
            })
        } catch (e) {
            throw e;
        }

    }

    async function register(call) {
        let userData;
        try {
            
            userData = makeUser({...call.request, hash_password: true})
            const user = await authModel.add(userData)
            if (!user)
                return getHttpError({statusCode: 500, message: "Internal error"})
            return getHttpResponse({
                data: {
                    id: user._id,
                    username: user.username,
                    accessToken: encode({id: user._id})
                }
            })
        } catch (e) {
            return getHttpError({statusCode: 500, message: e.message ? e.message : e})
        }
    }

}