import makeUser from '../user/user.js'

export default function authModel({ database }) {
    return Object.freeze({
        checkUsernamePassword,
        add,
        getUserByUsername
    })
    async function checkUsernamePassword({ username, password }) {
        const user = (await database.collection('users')
            .findOne({ username: username, password: password }))
        if (!user)
            return null
        return makeUser(user)
    }
    async function getUserByUsername({ username }) {
        const user = await database
            .collection('users')
            .findOne({ username: username })
        if (!user)
            return null
        return makeUser(user)
    }
    async function add(userInfo) {
        userInfo = Object.assign({}, userInfo)
        const { result, ops } = (await database.collection("users").insertOne(userInfo))
        if (result.ok === 1)
            return makeUser(ops[0])
        return null
    }
}