import protoLoader from '@grpc/proto-loader'
import grpc from 'grpc'
import createAuthCallback from "./auth.callback.js";
import getDb from "../../db/index.js";
import authModel from "./auth.model.js";
import {serviceProcessor} from "../../helpers/reqest-processor.js";

const packageDefinition = protoLoader.loadSync('src/proto/auth.proto')
const auth_proto = grpc.loadPackageDefinition(packageDefinition).authentication
const model = {authModel: authModel({database: await getDb()})}
const authCallback = createAuthCallback(model)

export default {
    service: auth_proto.AuthService.service,
    callbacks: {
        login: serviceProcessor(authCallback.login),
        register: serviceProcessor(authCallback.register),
    }
}