import required from '../../helpers/required.js'
import { validateUsername } from '../../helpers/validators.js'
import { hash } from '../../helpers/password-hasher.js'

export default function makeUser(userInfo = required('userInfo')) {
    const validatedUser = validate(userInfo)
    const normalizedUser = normalize(validatedUser)
    return Object.freeze(normalizedUser)
    function validate({
        username = required("username"),
        password = required("password"),
        ...extra
    }) {

        if (!validateUsername(username))
            throw 'Username is invalid'
        return { username, password, ...extra }
    }

    function normalize({
        username = required("username"),
        password = required("password"),
        hash_password = false,
        ...extra
    }) {
        username = username.toLowerCase()
        if (hash_password)
            password = hash(password)
        return { username, password, ...extra }
    }
}