import getHttpError from "../../helpers/http-error.js";
import getHttpResponse from "../../helpers/http-response.js";


export function makeListHandler({productModel}) {
    return async function handle(req) {
        // check if authenticated..
        const list = await productModel.getList()
        return getHttpResponse({statusCode: 200, data: {products: list}})
    }
}

export function makeSingleProductHandler({productModel}) {
    return async function handle(req) {
        console.log("single")
        switch (req.method) {
            case 'GET':
                // send back the details
                return null
            case 'POST':
                // create a new item
                return null
            case 'PUT':
                // update a product
                return null
            case 'DELETE':
                // delete a product
                return null
            default:
                return getHttpError({statusCode: 405, message: "not allowed"})
        }
    }

}