import required from '../../helpers/required.js'


export default function makeProduct(productInfo=required("required"))
{
    const validatedProduct = validate(productInfo)
    return Object.freeze(validatedProduct)

    function validate({
        title = required("title"),
        description = required("description"),
        image = required("image"),
        ...extra
    }){
        return {title, description, image,...extra}
    }
}