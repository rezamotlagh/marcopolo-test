import makeProduct from './product.js'

export default function productModel({ database }) {

    return Object.freeze({
        getList,
    })

    async function getList({ limit = 10, page = 1 } = {}) {
        const skip_count = (page - 1) * limit
        return  ( await database.collection("products")
            .find({})
            .skip(skip_count)
            .limit(limit)
            .toArray())
            .map(makeProduct)

    }
}