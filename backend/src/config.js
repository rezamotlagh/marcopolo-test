

export const MONGODB_URL = process.env.DATABASE_URL || 'mongodb://localhost:27017/marco'
export const DB_NAME = process.env.DB_NAME || 'marco'
export const JWT_SECRET = process.env.JWT_SECRET || 'some secret key here'
