import {MONGODB_URL, DB_NAME} from '../config.js'
import mongodb from 'mongodb'

const MongoClient = mongodb.MongoClient

export default async function getDb(){
    const client = new MongoClient(MONGODB_URL)
    await client.connect()
    return client.db(DB_NAME)
}