import {check} from '../helpers/jwt.js'
import getHttpError from "../helpers/http-error.js";

export function authenticationRequired(req, res, next) {
    try {
        const data = check(req.headers)
        if (!data) {
            const {statusCode, data, headers} = getHttpError({
                statusCode: 401,
                message: "Authentication credentials required."
            })
            res.set(headers).status(statusCode).send(data)
            return;
        }
        req.user_id = data.id
    } catch (e) {
        const {statusCode, data, headers} = getHttpError({statusCode: 401, message: e.message ? e.message : e})
        res.set(headers).status(statusCode).send(data)
        return;
    }
    next()
}