import jwt from 'jsonwebtoken'
import {JWT_SECRET} from '../config.js'

export default function encode(data) {
    return jwt.sign(data, JWT_SECRET, {expiresIn: "1D"})
}

export function check({authorization=""}) {
    const token = authorization.replace("Bearer ","")
    if(!token){
        throw "Authorization credentials is not provided."
    }
    const verified_data = jwt.verify(token,JWT_SECRET)
    let ts = Date.now()/ 1000
    if(verified_data.exp <= ts)
        throw 'token expired'
    return verified_data
}