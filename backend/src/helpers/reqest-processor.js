import grpc from 'grpc'

export const processRequest = (handler) => (req, res) => {
    handler(req)
        .then(({headers, statusCode, data}) => res.set(headers).status(statusCode).send(data))
        .catch(e => {
            console.log(e)
            res.status(500).end()
        })
}

export const serviceProcessor = (handler) => (call, callback) => {
    console.log("handler service")
    handler(call)
        .then(({header, statusCode, data}) => {
            if (data.success){
                callback(null, data.data)
            }else{
                callback({
                    code: statusCode,
                    message: data.message,
                    status: grpc.status.INTERNAL
                })
            }
        })
        .catch(e => {
            callback({
                code: 500,
                message: "internal error",
                status: grpc.status.INTERNAL
            })
        })
}