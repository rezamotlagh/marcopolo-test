export default function required(keyName){
    throw `${keyName} is required.`
}