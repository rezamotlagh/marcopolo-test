export default function getHttpError ({ statusCode, message }) {
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode,
      data: {
        success: false,
        message
      }
    }
  }