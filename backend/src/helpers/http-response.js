export default function getHttpResponse ({ statusCode=200, data }) {
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode,
      data: {
        success: true,
        data: data
      }
    }
  }