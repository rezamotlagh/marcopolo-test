import server from './App.js'
import grpc from "grpc";
import Auth from './modules/auth/auth.services.js'



server.addService(Auth.service, Auth.callbacks)

// create with insecure credentials for dev purpose
server.bind('0.0.0.0:9090', grpc.ServerCredentials.createInsecure());
server.start()
